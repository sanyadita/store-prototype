var currentProduct;
var currentProductPrice;

function addToCart() {
	var newCount = parseInt(document.getElementById('product-qty').value);
	if (newCount > 0) {
		addProductCount(currentProduct, newCount, currentProductPrice);
		showMessage("The product was successfully added to cart");
	}
	console.log(localStorage);
}

function addProductCount(name, count, price) {
	var newCount = 0;
	var items = localStorage.getItem("items");
	var list = {};
	if (items != null) {
		list = JSON.parse(items);
	}
	if (name.isUndefined) {
		return newCount;
	}
	if (name in list) {
		var item = list[name];
		newCount = item.qty + count;
		if (newCount <= 0) {
			return item.qty;
		}
		item.qty = newCount;
	} else {
		newCount = count;
		list[name] = {qty: count, price: price};
	}
	localStorage.setItem("items", JSON.stringify(list));
	onStorage();
	return newCount;
}

function changeCountByOne(caller, incBy) {
	var tableRow = caller.parentElement.parentElement.parentElement;
	var productName = tableRow.querySelector('#cart-item-name').textContent;
	var count = addProductCount(productName, incBy, 0);
	tableRow.querySelector('#cart-item-qty').textContent = count;
	countCartProducts();
}

function incProductCount(caller) {
	changeCountByOne(caller, 1);
}

function decProductCount(caller) {
	changeCountByOne(caller, -1);
}

function changeProductQty(caller) {
	var tableRow = caller.parentElement.parentElement.parentElement;
	var productName = tableRow.querySelector('#cart-item-name').textContent;
	var qty = parseInt(tableRow.querySelector('#cart-item-qty').value);
	if (qty > 0) {
		var items = localStorage.getItem("items");
		var list = {};
		if (items != null) {
			list = JSON.parse(items);
		}
		if (productName in list) {
			var item = list[productName];
			item.qty = qty;
		}
		localStorage.setItem("items", JSON.stringify(list));
		countCartProducts();
	} else {
		caller.value = caller.oldValue;
	}
}

function deleteProductFromCart(caller) {
	var tableRow = caller.parentElement.parentElement;
	var productName = tableRow.querySelector('#cart-item-name').textContent;
	var items = localStorage.getItem("items");
	var list = {};
	if (items != null) {
		list = JSON.parse(items);
	}
	if (productName in list) {
		delete list[productName];
	}
	localStorage.setItem("items", JSON.stringify(list));
	var table = tableRow.parentElement;
	table.removeChild(tableRow);
	countCartProducts();
	onStorage();
}

function countCartProducts() {
	var count = 0;
	var subtotal = 0;
	var currency = "";
	var list = JSON.parse(localStorage.getItem("items"));
	for (var itemName in list) {
		var item = list[itemName];
		currency = item.price.substr(0,1);
		var price = parseFloat(item.price.substr(1));
		if (!isNaN(item.qty)) {
			count += item.qty;
			if (!isNaN(price)) {
				subtotal += price * item.qty;
			}
		}
	}
	document.getElementById('cart-total-count').innerHTML = count;
	document.getElementById('cart-subtotal').innerHTML = currency + subtotal;
}

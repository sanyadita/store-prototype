var doc = document.currentScript.ownerDocument;

/** 
 * Reads products into interface with pagination.
 * 
 * @mathod fillListByProducts
 * @param {string} filename name of source file to read products
 */
function fillListByProducts(filename) {
	$(doc).ready(function() {
		$.getJSON(filename, function(data) {
			preparePaginator(data);
		});
	});
}

/** 
 * Prepares pagination with selected list.
 * 
 * @mathod preparePaginator
 * @param {Object[]} source
 */
function preparePaginator(source) {
	$('#pagination-container').pagination({
		dataSource: source,
		pageSize: 10,
		callback: function(data, pagination) {
			var html = template(data);
			$('#data-container').html(html);
			setListenersToTitles();
		}
	});
}

/**
 * Configures product items to open product deltails.
 *
 * @method setListenersToTitles
 */
function setListenersToTitles() {
	$(".product-item-link").click(function() {
		var name = $(this).data('name');
		var image = $(this).data('image');
		var price = $(this).data('price');
		$.ajax({
			type: 'GET',
			url: "views/product.html",
			dataType: 'html',
			success: function(data) {
				openPage(PAGE_TYPE_DETAIL, data, function() {
					load(name, image, price);
				});
			}
		});
	});
}

/**
 * Puts data to template and adds it to full list.
 *
 * @method template
 * @param {Object[]} data list of products
 */
function template(data) {
	var html = "";
	$.each(data, function(index, product) {
		var templ = $(doc).find("#product-template").html().trim();
		var clone = $(templ);

		var link = clone.find("#product-link");
		link.html(product.name);
		link.attr('data-name', product.name);
		link.attr('data-image', product.image);
		link.attr('data-price', product.price);
		
		clone.find("#product-price").html(product.price);
		clone.find("#product-photo").attr('src', product.image);
		html += clone.html();
	});
	return html;
}
/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        app.receivedEvent('deviceready');
    },
    // Update DOM on a Received Event
    receivedEvent: function(id) {
        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');

        console.log('Received Event: ' + id);
    }
	
};

/**
 * Constants for page types.
 */
const PAGE_TYPE_MAIN = 'M';
const PAGE_TYPE_DETAIL = 'D';

/**
 * Information about opened page: type, page code, function, that is called after loading the page, and
 * page which is previous for current.
 * 
 * @namespace
 */
var PAGE = {
	// Type of page level.
	pageType: PAGE_TYPE_MAIN,
	// Page title, which is shown on action bar.
	title: '',
	// Bode of current page.
	htmlCode: '',
	// To save scroll state of page.
	scrollY: 0,
	// Function calls after page was loaded.
	successFunction: function() {},
	// Link to previous page.
	previousPage: {}
};

/**
 * Current opened page.
 *
 * @property currentLevel
 */
var currentLevel = PAGE;

/**
 * User that is signed in in application.
 *
 * @property currentUser
 */
var currentUser;


// ======================================= EVENTS =======================================

/**
 * Should be called when the device is ready. 
 *
 * @method onDeviceReady
 */
function onDeviceReady() {
    document.addEventListener("backbutton", back, false);
	if (checkConnection()) {
		onOnline();
	}
}

/**
 * Should be called if internet connection is enabled. 
 *
 * @method onOnline
 */
function onOnline() {
	$(document).ready(function() {
		$('#first').click();
	});
}

/**
 * Should be called if state of cart was changed. 
 *
 * @method onStorage
 */
function onStorage() {
    var cart = document.querySelector('#shopping-cart');
	var list = JSON.parse(localStorage.getItem("items"));
	if (list != null) {
		var count = Object.keys(list).length;
		if (count == 0) {
			delete cart.dataset.badge;
		} else {
			cart.dataset.badge = count;
		}
	}
}


// ======================================= PAGE ACTIONS =======================================

/**
 * Opens previoius page.
 *
 * @method back
 */
function back() {
	if (currentLevel.pageType == PAGE_TYPE_MAIN) {
		navigator.app.exitApp();
		return;
	} 
	currentLevel = Object.assign({}, currentLevel.previousPage);
	if (currentLevel.pageType == PAGE_TYPE_MAIN) {
		prepareMainLevel();
		setTitle(currentLevel.title);
		window.scrollTo(0, currentLevel.scrollY);
		$('#detail-content').html('');
	} else {
		openCodeInPage();
	}
}

/**
 * Opens page in main page container. Sets current page as previous for new and saves
 * th state of scoll.
 *
 * @method openPage
 * @param {String} levelChar can be one of constants: PAGE_TYPE_MAIN or PAGE_TYPE_DETAIL.
 * @param {String} htmlCode code of selected page.
 * @param {Function} actions, that shoud be executed after page is loaded.
 */
function openPage(levelChar, htmlCode, successFunction) {
	var previousLevel = Object.assign({}, currentLevel);
	previousLevel.title = document.title;
	previousLevel.scrollY = window.scrollY;
	currentLevel = Object.assign({}, PAGE);
	currentLevel.previousPage = previousLevel;
	currentLevel.pageType = levelChar;
	currentLevel.htmlCode = htmlCode;
	currentLevel.successFunction = successFunction;
	
	openCodeInPage();
} 

/**
 * Insert code of current page into container
 *
 * @method openCodeInPage
 */
function openCodeInPage() {
	window.scrollTo(0, 0);
	switch (currentLevel.pageType) {
		case PAGE_TYPE_MAIN:
			prepareMainLevel();
			$('#main-content').html(currentLevel.htmlCode);
			break;
		case PAGE_TYPE_DETAIL:
			prepareDetailLevel();
			$('#detail-content').html(currentLevel.htmlCode);
			break;
	}
	if (!(typeof(componentHandler) == 'undefined')) {
		componentHandler.upgradeAllRegistered();
	}
	currentLevel.successFunction();
}


// ======================================= UI =======================================

/**
 * Prepares inteface to show  page on main level.
 *
 * @method prepareMainLevel
 */
function prepareMainLevel() {
	showMenuDrawerButton();
	document.getElementById('main-content').style.display = '';
	document.getElementById('detail-content').style.display = 'none';
}

/**
 * Prepares inteface to show  page on detail level (enables button back instead of button to open drawer).
 *
 * @method prepareDetailLevel
 */
function prepareDetailLevel() {
	showMenuBackButton();
	document.getElementById('main-content').style.display = 'none';
	document.getElementById('detail-content').style.display = '';
}

/**
 *
 * @method prepareMenu
 */
function prepareMenu() {
	var text = menu.import.querySelector('#nav_menu');
	document.getElementById('nav_menu').appendChild(text);
	onStorage();
}

/**
 * Hides button for open drawer menu and show button BACK.
 *
 * @method showMenuBackButton
 */
function showMenuBackButton() {
//	document.getElementById('menu-open').style.display = 'none';
//	document.getElementById('menu-back').style.display = '';
}

/**
 * Hides button back and shows button for open DRAWER menu.
 *
 * @method showMenuDrawerButton
 */
function showMenuDrawerButton() {
//	document.getElementById('menu-back').style.display = 'none';
//	document.getElementById('menu-open').style.display = '';
}
	
/**
 * Sets the title of the page.
 *
 * @method setTitle
 * @param {String} title the title that is shown on action bar.
 */
function setTitle(title) {
	currentLevel.title = title;
	var titleBar = document.getElementById('status_bar_title');
	if (titleBar!= null) titleBar.innerHTML = title;
	document.title = title;
}

/**
 * Shows snackbar with selected message.
 *
 * @method showMessage
 * @param {String} message
 */
function showMessage(message) {
	var notification = document.querySelector('.mdl-js-snackbar');
	notification.MaterialSnackbar.showSnackbar({
		message: message
	});
}

/**
 * Shows the dialog that internet connection is off.
 *
 * @method showDialog
 */
function showDialog() {
	var dialog = document.querySelector('dialog');
	if (! dialog.showModal) {
      dialogPolyfill.registerDialog(dialog);
    }
	$('#dialog-button-ok').click(function() {
		navigator.app.exitApp();
	});
	dialog.showModal();
}

/**
 * Exapands caller menu item and hdes previous.
 *
 * @method expand
 * @param {Object} caller element of page that calls this method
 * @param {String} targetId id of list that show be shown
 */
function expand(caller, targetId) {
	if (document.getElementById) {
		target = document.getElementById(targetId);
		if (target.style.display == "none") {
			$('.expandable-list').hide();
			target.style.display = "";
		} else{
			target.style.display = "none";
		}	
	}
}

function closeDrawer() {
	var drawer = document.querySelector('.mdl-layout__drawer');
	var classList = drawer.classList;
	if (classList.contains('is-visible')) {
		let d = document.querySelector('.mdl-layout');
		d.MaterialLayout.toggleDrawer();
	}
}

/** 
 * Prepares page links to open content.
 * 
 * @method prepareActions
 */
function prepareActions() {
	$('.nav-link').click(function() {
		var url = "views/" + $(this).data('content') + ".html";
		var mainVisible = document.getElementById('main-content').style.display != 'none';
		var isProductList = url == "views/product-list.html";
		if (isProductList || url != lastContent || !mainVisible) {
			lastContent = url;
			var listSource = $(this).data('source');
			var title = $(this).data('title');
			var loadFunc = $(this).data('load');
			$.ajax({
				type: 'GET',
				url: url,
				dataType: 'html',
				success: function(data) {
					openPage(PAGE_TYPE_MAIN, data, function() {
						if (isProductList) {
							loadProductList(title, listSource);
						}
						if (loadFunc != null) {
							window[loadFunc]();
						}
					});
					
				}
			});
			closeDrawer();
		}
	});
} 

// ======================================= DEVICE =======================================

/**
 * @method checkConnection
 * @return {Boolean} returns true if internet connection is enabled
 */
function checkConnection() {
    var networkState = navigator.connection.type;
	if (networkState == Connection.NONE) {
		showDialog();
		return false;
	}
	return true;
}


// ======================================= DATA =======================================

/**
 * Checks if user is signed in and hides buttons for sign in and create account. 
 *
 * @method checkUser
 */
function checkUser() {
	currentUser = localStorage.getItem('current-email');
	if (currentUser != null) {
		document.getElementById('sign-in-links').style.display = 'none';
		document.getElementById('account-link').style.display = '';
	}
}

/**
 * Reads list of products from selected file
 *
 * @method readProducts
 * @param {String} filename source file name
 * @param {Function} callback to show on of found element
 */
function readProducts(filename, onReadProduct) {
	$.getJSON(filename, function(data) {
		$.each(data, function(key, value) {
			onReadProduct(value);
		});
	});
}


// ======================================= 

document.addEventListener("deviceready", onDeviceReady, false);
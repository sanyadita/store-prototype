/**
 * Tries to sign in by data from iput with id fSinginEmail and fSigninPassword.
 * 
 * @method signin
 */
function signin() {
	var fSinginEmail = document.getElementById('fSinginEmail');
	var fSigninPassword = document.getElementById('fSigninPassword');
	if (isFieldValid(fSinginEmail, fSigninPassword)) {
		localStorage.setItem('current-email', fSinginEmail.value);
		location.reload();
	}	
}

/**
 * Tries to create new account. Show snackbar with error, if fails.
 * 
 * @method createAccount
 */
function createAccount() {
	var fFirstName = document.getElementById('fFirstName');
	var fLastName = document.getElementById('fLastName');
	var fEmail = document.getElementById('fEmail');
	var fPassword = document.getElementById('fPassword');
	var fRepeatPassword = document.getElementById('fRepeatPassword');
	
	if (isFieldValid(fFirstName, fLastName, fEmail, fPassword, fRepeatPassword)) {
		if (fPassword.value == fRepeatPassword.value) {
			localStorage.setItem('current-email', fEmail.value);
			location.reload();
		} else {
			showMessage('Passwords not equal');
		}
	}
}

/**
 * Checks if all selected fields have valid values.
 *
 * @method isFieldValid
 * @param {Object[]} fields list of input objects
 * @return {Boolean} returns false if any of selected field has invalid value
 
 */
function isFieldValid(...fields) {
	if (fields != null) {
		for (var i in fields) {
			if (!fields[i].checkValidity()) return false;
		}
		return true;
	}
	return false;
}

/**
 * Sign out current user.
 *
 * @method logout
 */
function logout() {
	localStorage.removeItem('current-email');
	location.reload();
}
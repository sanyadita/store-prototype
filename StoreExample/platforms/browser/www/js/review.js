function submitReview() {
	var nickname = document.getElementById('fNickname').value;
	var summary = document.getElementById('fSummary').value;
	var review = document.getElementById('fReview').value;
	var whatLike = document.getElementById('fWhatLike').value;
	var whatNotLike = document.getElementById('fWhatNotLike').value;
	var raiting = getRaiting();
	if (raiting < 1) {
		showMessage('Please, choose raiting');
	} else if (nickname.length == 0) {
		showMessage('Input your nickname');
	} else if (summary.length == 0) {
		showMessage('Input summary');
	} else if (review.length == 0) {
		showMessage('Input review message');
	} else {
		var reviewList = document.getElementById('reviews');
		
		var templ = document.querySelector("#review-template");
		var clone = document.importNode(templ.content, true);
		
		clone.querySelector("#review-author").textContent = nickname;
		clone.querySelector("#review-date").textContent = getCurrentDate();
		clone.querySelector("#review-summary").textContent = summary;
		clone.querySelector("#review-message").textContent = review;

		getInitials(nickname, clone.querySelector("#profile-image"));
		
		switch (raiting) {
			case 1:
				clone.querySelector("#star1").checked = true;
				break;
			case 2:
				clone.querySelector("#star2").checked = true;
				break;
			case 3:
				clone.querySelector("#star3").checked = true;
				break;
			case 4:
				clone.querySelector("#star4").checked = true;
				break;
			case 5:
				clone.querySelector("#star5").checked = true;
				break;
		}
		reviewList.insertBefore(clone, reviewList.firstChild);
	}
}

/**
 * Returns taing that user choosed in number format. Returns -1, if user didn't choose raiting.
 * 
 * @method getRaiting
 * @return {Integer} value form -1 to 5.
 */
function getRaiting() {	
	var star1 = document.getElementById('raiting1').checked;
	if (star1) {
		return 1;
	}
	var star2 = document.getElementById('raiting2').checked;
	if (star2) {
		return 2;
	}
	var star3 = document.getElementById('raiting3').checked;
	if (star3) {
		return 3;
	}
	var star4 = document.getElementById('raiting4').checked;
	if (star4) {
		return 4;
	}
	var star5 = document.getElementById('raiting5').checked;
	if (star5) {
		return 5;
	}
	return -1;
}

/** 
 * Converts current date to string. Returns date as mm/dd/yyyy.
 *
 * @method getCurrentDate
 * @return {String} current date in string format
 */
function getCurrentDate() {
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth() + 1; //January is 0!
	var yyyy = today.getFullYear();
	if (dd < 10) {
		dd = '0' + dd;
	} 
	if (mm < 10) {
		mm = '0' + mm;
	} 
	return mm + '/' + dd + '/' + yyyy;
}

function getInitials(name, divObject) {
	var arrayOfStrings = name.split(' ');
	var letters = "";
	arrayOfStrings.forEach(function(str) {
		letters += str.substr(0, 1);
	});
	letters = letters.substr(0, 3);
	
	var backgroundColor = stringToColor(name);
	
	divObject.innerHTML = letters;
	divObject.style.backgroundColor = backgroundColor;
}

/**
 * Converts string to color.
 * 
 * @method stringToColor
 * @param {String} str string to convert 
 * @return {String} hex color
 */
function stringToColor(str) {
    var hash = 0;
    var color = '#';
    var i;
    var value;
    var strLength;
	
    if (!str) {
        return color + '333333';
    }
    strLength = str.length;
    for (i = 0; i < strLength; i++) {
        hash = str.charCodeAt(i) + ((hash << 5) - hash);
    }
    for (i = 0; i < 3; i++) {
        value = (hash >> (i * 8)) & 0xFF;
        color += ('00' + value.toString(16)).substr(-2);
    }
    return color;
};